import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

@Test
public class StringCalculatorTest {

    private StringCalculator stringCalculator;

    @BeforeTest
    public void init() {
        stringCalculator = new StringCalculator();
    }

    public void emptyStringReturnsZero() {
        stringCalculator = new StringCalculator();
        assertEquals(stringCalculator.Add(""), 0);
    }

    public void singleValue() {
        assertEquals(stringCalculator.Add("1"), 1);
        assertEquals(stringCalculator.Add("15"), 15);
    }

    public void twoValueWithComma() {
        assertEquals(stringCalculator.Add("1,2"), 3);
        assertEquals(stringCalculator.Add("10,12"), 22);
    }

    public void multiValueWithComma() {
        assertEquals(stringCalculator.Add("1,2,3"), 6);
        assertEquals(stringCalculator.Add("1,2,3,4"), 10);
    }

    public void multiValueWithCommaAndNewLine() {
        assertEquals(stringCalculator.Add("1\n2"), 3);
        assertEquals(stringCalculator.Add("1\n2,3"), 6);
        assertEquals(stringCalculator.Add("1,2,3\n4"), 10);
    }

    public void valueWithDelimiter() {
        assertEquals(stringCalculator.Add("//.\n1.2"), 3);
        assertEquals(stringCalculator.Add("//;\n1;2;3"), 6);
    }

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void negativeValue() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("negatives not allowed: -1");

        stringCalculator.Add("-1");
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void multiNegativeValue() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("negatives not allowed: -1,-2,-3");

        stringCalculator.Add("5,-1,-2,3,-3");
    }

    public void addCalledCount() {
        assertEquals(stringCalculator.Add(""), 0);
        assertEquals(stringCalculator.Add("//;\n1;2;3"), 6);
        assertEquals(stringCalculator.GetCalledCount(), 2);
    }

    public void biggerNumberIgnore() {
        assertEquals(stringCalculator.Add("1001"), 0);
        assertEquals(stringCalculator.Add("1001,2\n3\n4,5"), 14);
        assertEquals(stringCalculator.Add("//;\n1000;2000;3000"), 1000);
    }

    public void valueWithBigDelimiter() {
        assertEquals(stringCalculator.Add("//[$$]\n1$$2$$3"), 6);
        assertEquals(stringCalculator.Add("//[***]\n1***2***3"), 6);
    }

    public void valueWithMultiDelimiter() {
        assertEquals(stringCalculator.Add("//[*][%]\n1*2%3"), 6);
        assertEquals(stringCalculator.Add("//[-][*]\n1-2*3"), 6);
    }

    public void valueWithMultiBigDelimiter() {
        assertEquals(stringCalculator.Add("//[**][%%]\n1**2%%3"), 6);
        assertEquals(stringCalculator.Add("//[--][...]\n1--2...3"), 6);
    }
}