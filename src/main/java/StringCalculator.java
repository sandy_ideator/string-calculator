import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class StringCalculator {

    private String number;
    private String delimiter;
    private int addCalledCount = 0;

    private void init(String delimiter, String number) {
        this.delimiter = delimiter;
        this.number = number;
    }

    int Add(String numbers) {
        addCalledCount++;
        if (isEmpty(numbers)) {
            return 0;
        }
        parseNumbers(numbers);
        return findSum();
    }

    private void parseNumbers(String numbers) {
        if (numbers.startsWith("//")) {
            String[] split = numbers.split("\n", 2);
            String delimiter = parseDelimiter(split[0]);
            init(delimiter, split[1]);
        } else {
            init(",|\n", numbers);
        }
    }

    private static String parseDelimiter(String header) {
        String delimiter = header.substring(2);
        if (delimiter.startsWith("[")) {
            delimiter = delimiter.substring(1, delimiter.length() - 1);
        }
        return Stream.of(delimiter.split("]\\["))
                .map(Pattern::quote)
                .collect(Collectors.joining("|"));
    }

    private int findSum() {
        String[] nums = getNumberArray();
        findNegativeValue(nums);
        return sum(nums);
    }

    private String[] getNumberArray() {
        return number.split(delimiter);
    }

    private void findNegativeValue(String[] nums) {
        StringBuilder negativeValues = new StringBuilder();
        for (String num : nums) {
            int value = stringToInt(num);
            if (value < 0) {
                negativeValues.append(value).append(",");
            }
        }
        if (negativeValues.length() > 0) {
            throw new IllegalArgumentException("negatives not allowed: " + negativeValues.substring(0, negativeValues.length() - 1));
        }
    }

    private int sum(String[] nums) {
        int sum = 0;
        for (String num : nums) {
            int numValue = stringToInt(num);
            if (numValue > 1000) {
                continue;
            }
            sum += numValue;
        }
        return sum;
    }

    int GetCalledCount() {
        return addCalledCount;
    }

    private boolean isEmpty(String numbers) {
        return numbers.isEmpty();
    }

    private int stringToInt(String number) {
        return Integer.parseInt(number);
    }
}
